#!/usr/bin/env node
/**
 * https://github.com/tj/commander.js/
 */
const { PyBabelBridge } = require("./../src/pybabel/bridge");
const program = require("commander");
const { Config } = require("./../src/config/config");
const debug = require("debug")("pybabel-express:cli");


debug("pybabel-express cli");

program
    .version("1.0.0")
    .usage("pybex [cmd] [options]");

program
    .command("extract")
    .description("extract the translatable strings from given sources")
    .action(async function() {
        console.log("extract");
        new PyBabelBridge(Config.fromFile()).extract()
            .then(()=>true)
            .catch(err => console.error(err));
    });

program
    .command("update")
    .description("update all .po files from .pot file")
    .action(function () {
        console.log("update");
        new PyBabelBridge(Config.fromFile())
            .update()
            .then(()=>true)
            .catch(err => console.error(err));
    });

program
    .command("init <locale>")
    .description("init new locale")
    .action(function (locale) {
        console.log("init " + locale);
        new PyBabelBridge(Config.fromFile())
            .init(locale)
            .then(()=>true)
            .catch(err => console.error(err));
    });

program
    .command("compile")
    .description("compiles all .po files to .mo files")
    .action(function () {
        console.log("compile");
        new PyBabelBridge(Config.fromFile())
            .compile()
            .then(()=>true)
            .catch(err => console.error(err));
    });


if ( ! process.argv.slice(2).length) {
    program.outputHelp();
}

program.on("command:*", function () {
    console.error("Invalid command: %s\nSee --help for a list of available " +
        "commands.", program.args.join(" "));
    process.exit(1);
});

program
    .parse(process.argv);