# pybabel-express

This is a bridge between pybabel commandline tool and a node

for it's installation see: 
    
    http://babel.pocoo.org/en/latest/cmdline.html
    
   
The following commands have been implemented in the Bridge
   
    pybabel extract -F babel.cfg -o locales/test.pot src/ test/
    pybabel init -i locales/test.pot -D test -d locales/ -l en
    pybabel update -i locales/test.pot -D test -d locales/
    pybabel compile -d locales/ -D test


## Installation
#### babel.cfg

    [javascript: **.js]

#### .pybexrc.json

    {
      "default_locale": "en"
      "domain": "application", 
      "destination": "locales", 
      "sources": ["src", "seeders", "config"],
    }



### cli tool

The corresponding values are read from the .pbybexrc.json file

    node_modules/.bin/pybex --help
    node_modules/.bin/pybex extract
    node_modules/.bin/pybex init <locale>
    node_modules/.bin/pybex update
    node_modules/.bin/pybex compile
    
### using the express middleware

You will need to create your global Translator instance. And add the
middleware to your express application.

```javascript
    /**
     * i18n.js
     * 
     * creating a global translation file
     */
    
    const debug = true;
    
    // creating the global Translator object
    const translator = Translator.createDefaultInstance(debug);
    module.exports = translator;
```

```javascript
    /**
     * server.js
     * 
     * including the middleware into your express application
     */
    const express = require("express");
    const { TranslatorMiddleware } = require("pybabel-express");
    const translator = require("./i18n.js");
    
    const app = express();
    
    // adding the middleware
    app.use(new TranslatorMiddleware().bind(translator));
    
    app.get("/helloworld", async (req, res) => {
        res.status(200).send({
            "text": req.translator.gettext("Hello World")
        });    
    });
    
    ...
    
    app.listen(8000)

```