const debug = require("debug")("pybabel-express:translator:translator");
const Gettext = require("node-gettext");
const { MoFileReader } = require("./../pybabel/gettext");
const { DirectoryLocaleReader } = require("../pybabel/locales");
const { Config } = require("../config/config");
const { DetectLocale, AcceptLanguageParser } = require("./detect-locale");


const gMoFileCache = new Map();



class NullTranslator {

    constructor(sanitizer, default_locale="en") {
        this.sanitizer = sanitizer;
        this.default_locale = default_locale;
        this.current_locale = default_locale;
    }

    useLocale(locale) {
        this.current_locale = locale;
        return this.getLocale();
    }

    getLocale() {
        return this.current_locale;
    }

    gettext(msgid) {
        return msgid;
    }

    ngettext(msgid, msgid2, n) {
        return ((n === 1) ? msgid : msgid2).replace("%d", n);
    }
}

class GettextTranslator extends NullTranslator {

    constructor(sanitizer, domain, locale_dir, default_locale, debug) {
        super(sanitizer, default_locale);
        this.domain = domain;
        this.default_locale = default_locale;
        this.directory = locale_dir;
        this.gt = new Gettext({ "debug": !!debug });
        this.gt.setTextDomain(this.domain);
        this.localeReader = new DirectoryLocaleReader(this.directory);
        this.useLocale(this.default_locale);
    }

    /**
     * Creates a Translator instance
     * @param sanitizer
     * @param config
     * @param debug
     * @returns {GettextTranslator}
     */
    static createFromConfig(sanitizer, config, debug=false) {
        config.validate();
        return new GettextTranslator(
            sanitizer,
            config.domain,
            config.destination,
            config.default_locale,
            debug
        );
    }

    /**
     * Creates the default Translator instance from default config file and
     * paths
     * @param sanitizer
     * @param debug
     * @returns {GettextTranslator}
     */
    static createDefaultInstance(sanitizer, debug=false) {
        const config = Config.fromFile();
        return GettextTranslator.createFromConfig(sanitizer, config, debug);
    }

    useLocale(locale) {

        let parser = new AcceptLanguageParser();
        let detector = new DetectLocale(
            this.localeReader.getLocales(),
            this.default_locale,
            parser
        );

        locale = detector.detect(locale);
        locale = this.sanitizer.sanitize(locale);

        if ( ! (this.domain in this.gt.catalogs)
            || ! (locale in this.gt.catalogs[this.domain])) {

            let hashKey = `${this.directory}-${locale}-${this.domain}`;

            if ( ! gMoFileCache.has(hashKey)) {
                let reader = new MoFileReader();
                let moFile = reader.resolve(this.directory, locale, this.domain);

                try {
                    let mo = reader.read(moFile);
                    gMoFileCache.set(hashKey, mo);
                } catch (e) {
                    debug(".mo file cannot be read or parsed", moFile);
                }
            }
            let mo = gMoFileCache.get(hashKey);
            this.gt.addTranslations(locale, this.domain, mo);

        }

        this.gt.setLocale(locale);
        this.current_locale = locale;

        return locale;
    }

    gettext(msgid) {
        return this.gt.gettext(msgid);
    }

    ngettext(msgid, msgid2, n) {
        return this.gt.ngettext(msgid, msgid2, n);
    }
}


module.exports = {
    NullTranslator,
    GettextTranslator,
};