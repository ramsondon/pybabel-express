const debug = require("debug")("pybabel-express:translator:detect-language");

class DetectLocale {
    constructor(locales, default_locale, parser) {
        this.supported_locales = new Set(locales); // available locales
        this.default_locale = default_locale;
        this.parser = parser;
    }

    /**
     *
     * @param value - accept language string
     * @returns {*}
     */
    detect(value) {

        if ( ! value) {
            // if we have no input just return the default locale
            return this.default_locale;
        }

        let locale_q_pairs = this.parser.parse(value);
        // search all accept languages for matching supported locales
        // locale_q_pairs should already be sorted by quality
        for (let i = 0; i < locale_q_pairs.length; i++) {
            let pair = locale_q_pairs[i];
            let locale = pair[0].replace("-", "_");

            if (locale) {
                let sup = this.supported_locales;
                if (sup.has(locale) || sup.has(pair[0])) {
                    debug("determined locale from accept-language header:",
                        locale);

                    return locale;
                }

                else if (sup.has(locale.substr(0,2))
                    || sup.has(pair[0].substr(0,2))) {
                    debug("determined locale from accept-language header:",
                        locale);

                    return locale.substr(0,2);
                }
            }
        }

        debug("Accept-Language did not match any locale. " +
            "Using configured fallback", this.default_locale);

        return this.default_locale;
    }
}

class AcceptLanguageParser {
    /**
     * https://siongui.github.io/2012/10/11/
     *      python-parse-accept-language-in-http-request-header/
     */
    parse(value) {
        // debug("trying to parse", value);
        let languages = (value.length > 0) ? value.split(",") : [];
        let locale_q_pairs = [];

        languages.forEach(language => {
            // trim all whitespace and commas or semicolons from right
            language = language.trim().replace(/[\s,;]*$/, "");
            let groups = language.split(";");
            if (groups[0] === language) {
                // no q => q = 15
                locale_q_pairs.push([language, 1.0]);
            } else {

                let locale = language.split(";")[0].trim();
                debug("AcceptLanguageParser: split", locale);
                let q = language.split(";")[1].split("=")[1].trim();
                locale_q_pairs.push([locale, parseFloat(q)]);
            }
        });

        return locale_q_pairs;
    }
}

module.exports = {
    DetectLocale,
    AcceptLanguageParser
};