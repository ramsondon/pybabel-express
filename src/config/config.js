const path = require("path");
const fs = require("fs");
const debug = require("debug")("pybabel-express:config:config");
const util = require("util");
const { LC_MESSAGES_DIR } = require("./../pybabel/gettext");

const DEFAULT_CONFIG_NAME = ".pybexrc.json";

class Config {

    constructor() {
        this.babel = "babel.cfg";
        this.sources = ["."];
        this.destination = "locales";
        this.domain = "application";
        this.default_locale = null;
    }


    static fromFile(file = DEFAULT_CONFIG_NAME) {
        let config = new Config();
        let cfgPath = path.resolve("./" + file);

        if ( ! fs.existsSync(cfgPath)) {
            throw new Error("the .pybexrc.json file is missing");
        }

        const cfg = require(cfgPath);
        debug(cfg);

        for (let k in cfg) {
            if (config.hasOwnProperty(k) && cfg.hasOwnProperty(k)) {
                config[k] = cfg[k];
            }
        }

        return config;
    }

    validate() {
        return this.validateLocale()
        && this.validateBabelCfg()
        && this.validateDomain()
        && this.validateSources();

    }

    validateDomain() {
        if ( ! this.domain) {
            throw new Error("please configure a domain in the .pybexrc.json");
        }
        return true;
    }

    validateLocale() {
        let p = util.format("./%s/%s/%s/%s.po",
            this.destination,
            this.default_locale,
            LC_MESSAGES_DIR,
            this.domain
        );
        p = path.resolve(p);
        debug("validateLocale", p);

        if ( ! fs.existsSync(p)) {
            let msg = util.format("default locale has not been initialized");
            throw new Error(msg);
        }

        return true;
    }

    validateBabelCfg() {
        if ( ! fs.existsSync(this.babel)) {
            let msg = util.format("the babel config file '%s' cannot be found",
                this.babel);
            throw new Error(msg);
        }

        return true;
    }

    validateSources() {
        this.sources.forEach(s => {
            if ( ! fs.existsSync("./" + s)) {
                let msg = util.format("the directory '%s' does not exist", s);
                throw new Error(msg);
            }
        });
        return true;
    }

    getDefaultLocale() {
        return this.default_locale;
    }

    getBabelCfg() {
        return this.babel;
    }

    getPotFile() {
        let fullpath = "./" + this.destination + "/" + this.domain + ".pot";
        return path.resolve(fullpath);
    }

    getSources() {
        return this.sources.map((v) => v.replace(/\/*$/, "") + "/").join(" ");
    }

    getDomain() {
        return this.domain;
    }

    getDestination() {
        return this.destination.replace(/\/*$/, "") + "/";
    }
}

module.exports = {
    Config,
};