const {AcceptLanguageParser, DetectLocale} = require("../translator/detect-locale");

class LocaleMiddleware {

    constructor(provider, sanitizer) {
        this.localeProvider = provider;
        this.sanitizer = sanitizer;
        this.locale = this.getDefaultLocale();
    }

    parseLanguage(locale) {
        let parser = new AcceptLanguageParser();
        let detector = new DetectLocale(
            this.localeProvider.getLocales(),
            this.localeProvider.getDefaultLocale(),
            parser
        );

        locale = detector.detect(locale);
        return this.sanitizer.sanitize(locale);
    }

    /**
     * Returns the list of locales ["de", "en", "en_US", "..."]
     * @returns {string[]}
     */
    getSupportedLocales() {
        return Array.from(this.localeProvider.getLocales());
    }

    getDefaultLocale() {
        return this.localeProvider.getDefaultLocale();
    }

    /**
     * Returns the locale "de", "it_CH", ...
     * @returns {string}
     */
    getLocale() {
        return this.locale;
    }

    bind() {
        return async (req, res, next) => {
            req.locale = this.parseLanguage(req.headers["accept-language"]);
            req.supported_locales = this.getSupportedLocales();
            this.locale = req.locale;
            if (next) next();
        };
    }
}

class TranslatorMiddleware {

    constructor() {
        this.useLocaleValue = false;
    }

    fromLocale() {
        this.useLocaleValue = true;
        return this;
    }

    readLocale(req) {
        if (this.useLocaleValue) {
            return req.locale || req.headers["accept-language"];
        }

        return req.headers["accept-language"];
    }

    bind(translator) {
        return async (req, res, next) => {
            let locale = this.readLocale(req);
            if (typeof(translator) === "function") {
                req.translator = translator();
            } else {
                req.translator = translator;
            }
            req.translator.useLocale(locale);
            if (next) next();
        };
    }
}

module.exports = {
    TranslatorMiddleware,
    LocaleMiddleware
};