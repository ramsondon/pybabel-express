const path = require("path");
const fs = require("fs");
// const debug = require("debug")("pybabel-express:translator:locales");
const { LC_MESSAGES_DIR } = require("./gettext");


class LocaleSanitizer {
    sanitize(locale) {
        return locale.replace("-", "_");
    }
}

class JsonLocaleReader {

    /**
     * Structure must be
     *  {
     *      default_locale: "en"
     *      locales: ["de", "en", "en_US"]
     *  }
     *
     * @param json
     */
    constructor(json, sanitizer) {
        this.json = json;
        this.parsed = false;
        this.locales = new Set();
        this.sanitizer = sanitizer;
        this.default_locale = null;
    }

    getLocales() {
        if ( ! this.parsed) {
            if ("default_locale" in this.json) {
                this.default_locale = this.json.default_locale;
            }
            if ("locales" in this.json) {
                this.parsed = true;
                this.json.locales.forEach(locale => {
                    this.locales.add(this.sanitizer.sanitize(locale));
                });
            }
        }

        return Array.from(this.locales);
    }

    getDefaultLocale() {
        return this.default_locale;
    }

}

class JsonFileLocaleReader {
    constructor(filename, sanitizer) {
        this.sanitizer = sanitizer;
        this.filename = filename;
        this.cache = null;
        this.reader = null;
    }

    getLocales() {
        if (null === this.cache) {
            this.cache = require(path.resolve(this.filename));
            this.reader = new JsonLocaleReader(this.cache, this.sanitizer);
        }

        return this.reader.getLocales();
    }

    getDefaultLocale() {
        this.getLocales();
        return this.reader.getDefaultLocale();
    }
}

class DirectoryLocaleReader {
    constructor(directory) {
        this.directory = directory;
        this.lcMessagesDir = LC_MESSAGES_DIR;
        this.binExtension = "mo";
    }

    resolve(locale, domain = null) {
        let dir = this.directory;
        let lcMsgDir = this.lcMessagesDir;
        let ext = this.binExtension;

        if (null === domain) {
            return path.resolve(`${dir}/${locale}/${lcMsgDir}`);
        }

        return path.resolve(`${dir}/${locale}/${lcMsgDir}/${domain}.${ext}`);
    }

    exists(locale, domain = null) {
        let p = this.resolve(locale, domain);
        return fs.existsSync(p);
    }

    getLocales() {
        let dir = this.directory;
        return fs.readdirSync(dir).filter(function (file) {
            try {
                return fs.lstatSync(`${dir}/${file}`).isDirectory();
            } catch (e) {
                return false;
            }
        });
    }

    getDefaultLocale() {
        let locales = this.getLocales();
        if (locales.length > 0) {
            return locales[0];
        }

        return null;
    }
}

module.exports = {
    LocaleSanitizer,
    JsonLocaleReader,
    JsonFileLocaleReader,
    DirectoryLocaleReader
};