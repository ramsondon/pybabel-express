const path = require("path");
const fs = require("fs");
const { mo } = require("gettext-parser");
const debug = require("debug")("pybabel-express:pybabel:gettext");

const LC_MESSAGES_DIR = "LC_MESSAGES";

class MoFileReader {

    constructor() {
        this.lcMessagesDir = LC_MESSAGES_DIR;
    }

    /**
     * Resolves the path to the .mo file
     *
     * @param locale_dir
     * @param locale
     * @param domain
     * @returns {*}
     */
    resolve(locale_dir, locale, domain) {
        let lcMsgDir = this.lcMessagesDir;
        let p = `${locale_dir}/${locale}/${lcMsgDir}/${domain}.mo`;
        let mofile = path.resolve(p);
        debug(mofile);

        return mofile;
    }

    /**
     * reads and parses the moFile
     * @param moFile
     * @returns {*}
     */
    read(moFile) {
        let binary = fs.readFileSync(moFile);
        return mo.parse(binary);
    }
}

module.exports = {
    MoFileReader,
    LC_MESSAGES_DIR,
};