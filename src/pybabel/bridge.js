const util = require("util");
const exec = util.promisify(require("child_process").exec);
// const { spawn = require("child_process");
const debug = require("debug")("pybabel-express:pybabel:bridge");
const fs = require("fs");

const BINARY = "pybabel";

class PyBabelBridge {

    constructor(config, binary=BINARY) {
        this.binary = binary;
        this.config = config;
    }

    /**
     * pybabel extract -F babel.cfg -o locales/test.pot src/ test/
     */
    extract() {
        return this.exec([
            "extract",
            "-F",
            this.config.getBabelCfg(),
            "--sort-output",
            "-o",
            this.config.getPotFile(),
            this.config.getSources()
        ]);
    }

    /**
     * pybabel update -i locales/test.pot -D test -d locales/
     */
    update() {
        return this.exec([
            "update",
            "-i",
            this.config.getPotFile(),
            "-D",
            this.config.getDomain(),
            "-d",
            this.config.getDestination(),
        ]);
    }

    /**
     * pybabel init -i locales/test.pot -D test -d locales/ -l en
     */
    init(locale) {
        return this.exec([
            "init",
            "-i",
            this.config.getPotFile(),
            "-D",
            this.config.getDomain(),
            "-d",
            this.config.getDestination(),
            "-l",
            locale.replace("-", "_"),
        ]);
    }

    /**
     * pybabel compile -d locales/ -D test
     */
    compile() {
        return this.exec([
            "compile",
            "-D",
            this.config.getDomain(),
            "-d",
            this.config.getDestination()
        ]);
    }

    async exec(cmd_options) {
        this.createDestinationDir();
        let cmd = [ this.binary ].concat(cmd_options).join(" ");
        debug(cmd);
        const {stdout, stderr } = await exec(cmd);

        if (stderr) {
            debug(stderr);
        }
        if (stdout) {
            console.log(stderr);
        }
    }

    createDestinationDir() {
        let dest = this.config.getDestination();
        if ( ! fs.existsSync(dest)) {
            debug("create directory", dest);
            fs.mkdirSync(dest);
        } else {
            debug("directory exists", dest);
        }

    }

}

module.exports = {
    PyBabelBridge
};
