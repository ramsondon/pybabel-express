const { GettextTranslator } = require("./translator/translator");
const { LocaleSanitizer, JsonFileLocaleReader } = require("./pybabel/locales");
const {
    LocaleMiddleware,
    TranslatorMiddleware
} = require("./express/middlewares");


class Factory {

    constructor() {
        this.sanitizer = new LocaleSanitizer();
    }

    /**
     * @param debug
     * @returns {GettextTranslator}
     */
    createGettextTranslator(debug=false) {
        return GettextTranslator.createDefaultInstance(this.sanitizer, debug);
    }


    createTranslatorMiddleware() {
        return new TranslatorMiddleware();
    }

    createLocaleMiddleware(configFile) {
        let reader = new JsonFileLocaleReader(configFile, this.sanitizer);
        return new LocaleMiddleware(reader, this.sanitizer);
    }
}


module.exports = {
    Factory
};