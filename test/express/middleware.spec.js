const http_mocks = require("node-mocks-http");
const { TranslatorMiddleware, LocaleMiddleware } = require("../../src/express/middlewares");
const { GettextTranslator } = require("../../src/translator/translator");
const { LocaleSanitizer, DirectoryLocaleReader, JsonLocaleReader } = require("../../src/pybabel/locales");


function mockResp() {
    return http_mocks.createResponse({
        eventEmitter: require("events").EventEmitter
    });
}

function mockReq(locale) {
    return http_mocks.createRequest({
        method: "GET",
        url: "/test/url",
        headers: {
            "accept-language": locale + ";",
        }
    });
}

describe("Express Locale Middleware Test Suite", () => {

    it("should pass the Locale middleware with requested language",
        async function () {

            const response = mockResp();
            const request = mockReq("it");

            const sanitizer = new LocaleSanitizer();
            let middleware = new LocaleMiddleware(new JsonLocaleReader({
                default_locale: "de",
                locales: ["en", "it", "de", "fr"]
            }, sanitizer), sanitizer);

            let controller = middleware.bind();
            await controller(request, response);

            middleware.getSupportedLocales().should.containEql("en");
            middleware.getSupportedLocales().should.containEql("it");
            middleware.getSupportedLocales().should.containEql("de");
            middleware.getSupportedLocales().should.containEql("fr");
            middleware.getSupportedLocales().should.not.containEql("ar");
            ("locale" in request).should.be.true();
            request.locale.should.equal("it");
        });

    it("should pass the Locale middleware with fallback language",
        async function () {

            const response = mockResp();
            const request = mockReq("it");

            const sanitizer = new LocaleSanitizer();
            let middleware = new LocaleMiddleware(new JsonLocaleReader({
                default_locale: "de",
                locales: ["en", "de", "fr"]
            }, sanitizer), sanitizer);

            let controller = middleware.bind();
            await controller(request, response);

            ("locale" in request).should.be.true();
            request.locale.should.equal("de");
        });

});

describe("Express Translation Middleware Test Suite", () => {

    const config = {
        domain: "test",
        locale_dir: "locales",
        default_domain: "de",
        debug: true
    };

    const translator = new GettextTranslator(
        new LocaleSanitizer(),
        config.domain,
        config.locale_dir,
        config.default_domain,
        config.debug
    );


    it("should pass the Translator middleware", async function () {
        const response = mockResp();
        const request = mockReq("it-CH");

        let middleware = new TranslatorMiddleware(new DirectoryLocaleReader());
        let controller = middleware.bind(translator);
        await controller(request, response);

        ("translator" in request).should.be.true();
        request.translator.getLocale().should.equal("de");
    });


    it("should pass the Translator middleware", async function () {
        const response = mockResp();
        const request = mockReq("en");

        let middleware = new TranslatorMiddleware();
        let controller = middleware.bind(translator);
        await controller(request, response);

        ("translator" in request).should.be.true();
        request.translator.getLocale().should.equal("en");
    });

});