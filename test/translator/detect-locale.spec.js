const {
    AcceptLanguageParser,
    DetectLocale
} = require("./../../src/translator/detect-locale");


describe("Accept Language Parser Test Suite", function () {
    const parser = new AcceptLanguageParser();

    it ("should parse a single locale", async function () {
        parser.parse("de-AT").should.deepEqual([
            ["de-AT", 1.0]
        ]);
    });

    it ("should test the accept language parser", async function () {

        parser.parse(
            "de-AT,de-DE;q=0.9,de;q=0.8,en-US;q=0.7,en;q=0.6"
        ).should.deepEqual(
            [
                ["de-AT", 1.0],
                ["de-DE", 0.9],
                ["de", 0.8],
                ["en-US", 0.7],
                ["en", 0.6],
            ]
        );

        parser.parse("es-mx, es, en").should.deepEqual([
            ["es-mx", 1.0],
            ["es", 1.0],
            ["en", 1.0]
        ]);
        parser.parse("de; q=1.0, en; q=0.5").should.deepEqual([
            ["de", 1.0],
            ["en", 0.5]
        ]);

        parser.parse("").should.deepEqual([]);
    });
});

describe("Detect Language Parser Test Suite", function () {
    const parser = new AcceptLanguageParser();
    const detector = new DetectLocale(
        ["de", "de_CH", "en", "it"], "it", parser
    );

    it ("should", async function () {
        detector.detect("de-AT,de-DE;q=0.9,de;q=0.8,en-US;q=0.7,en;q=0.6")
            .should.equal("de");
        detector.detect("es-mx, es, en").should.equal("en");
        detector.detect("de; q=1.0, en; q=0.5").should.equal("de");
        detector.detect("de-CH; q=1.0, de; q=0.9,en; q=0.5")
            .should.equal("de_CH");
        detector.detect("").should.equal("it");

        detector.detect("fr-CH, fr;q=0.9, en;q=0.8, de;q=0.7, *;q=0.5")
            .should.equal("en");
    });
});
