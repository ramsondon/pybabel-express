const { LocaleSanitizer } = require("./../../src/pybabel/locales");
const {
    NullTranslator,
    GettextTranslator
} = require("../../src/translator/translator");
const util = require("util");

describe("Translator Test Suite", function () {

    const config = {
        domain: "test",
        locale_dir: "locales",
        default_domain: "de",
        debug: true
    };

    const translator = new GettextTranslator(
        new LocaleSanitizer(),
        config.domain,
        config.locale_dir,
        config.default_domain,
        config.debug
    );

    const test_translations = {
        "en": "Hey this is a compiled text. en",
        "en_US": "Hey this is a compiled text. en_US",
        "de": "Hey this is a compiled text. de",
    };

    function get_message(trans) {
        return trans.gettext("Hey this is a compiled text.");
    }

    it("should read correct locales", async function () {

        translator.useLocale("en").should.equal("en");
        get_message(translator).should.equal(test_translations.en);

        translator.useLocale("en_US").should.equal("en_US");
        get_message(translator).should.equal(test_translations.en_US);

        translator.useLocale("de").should.equal("de");
        get_message(translator).should.equal(test_translations.de);

        translator.useLocale("de-AT").should.equal("de");
        get_message(translator).should.equal(test_translations.de);
    });

});


describe("NullTranslator Test Suite", function () {

    it("should sprintf the messages in gettext", async function () {
        let t = new NullTranslator();
        let text = t.gettext("I will buy %d eggs for %s");
        text.should.equal("I will buy %d eggs for %s");
        util.format(text, 10, "Stefanie")
            .should.equal("I will buy 10 eggs for Stefanie");
    });

    it("should sprintf the messages in ngettext", async function () {
        let t = new NullTranslator();
        let s_msg = "I will buy one egg for %s";
        let p_msg = "I will buy %d eggs for %s";

        let s1 = t.ngettext(s_msg, p_msg, 0);
        s1.should.equal("I will buy 0 eggs for %s");
        util.format(s1, "Stefanie")
            .should.equal("I will buy 0 eggs for Stefanie");

        let s2 = t.ngettext(s_msg, p_msg, 1);
        s2.should.equal("I will buy one egg for %s");

        let p1 = t.ngettext(s_msg, p_msg, 2);
        p1.should.equal("I will buy 2 eggs for %s");

        let p2 = t.ngettext(s_msg, p_msg, 10);
        p2.should.equal("I will buy 10 eggs for %s");
    });

    it("should sprintf the messages in ngettext reverse order", async () => {
        let t = new NullTranslator();
        let s_msg = "%s will buy %d egg.";
        let p_msg = "%s will buy %d eggs.";

        let s1 = t.ngettext(s_msg, p_msg, 0);
        s1.should.equal("%s will buy 0 eggs.");

        let s2 = t.ngettext(s_msg, p_msg, 1);
        s2.should.equal("%s will buy 1 egg.");

        let p1 = t.ngettext(s_msg, p_msg, 2);
        p1.should.equal("%s will buy 2 eggs.");

        let p2 = t.ngettext(s_msg, p_msg, 10);
        p2.should.equal("%s will buy 10 eggs.");
        util.format(p2, "Stefanie").should.equal("Stefanie will buy 10 eggs.");
    });

});