const { MoFileReader } = require("./../../src/pybabel/gettext");

describe("MoFileReader Test Suite", function () {

    it ("should resolve the .mo file path", async function () {
        let reader = new MoFileReader();
        let locale_dir = "locales";
        let locale = "de";
        let domain = "test";
        let resolved = reader.resolve(locale_dir, locale, domain);
        resolved.should.endWith("locales/de/LC_MESSAGES/test.mo");
    });

    it("should read the .mo file", async function () {
        let reader = new MoFileReader();
        let locale_dir = "locales";
        let locale = "de";
        let domain = "test";
        let content = reader.read(reader.resolve(locale_dir, locale, domain));
        ("translations" in content).should.be.true();
    });
});
