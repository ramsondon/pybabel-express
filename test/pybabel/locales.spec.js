const { DirectoryLocaleReader, JsonFileLocaleReader, LocaleSanitizer } = require("../../src/pybabel/locales");

describe("JsonFileReader test suite", function () {
    it("should read all locales", async function () {
        let locales = new JsonFileLocaleReader(
            "supported_languages.json",
            new LocaleSanitizer()
        );
        locales.getDefaultLocale().should.equal("en");
        locales.getLocales().should.deepEqual(["en", "de", "fr", "it"]);
    });
});

describe("DirectoryLocaleReader Test Suite", function () {
    it("should read all locales", async function () {
        let locale_dir = "locales";
        let locales = new DirectoryLocaleReader(locale_dir);
        locales.exists("en").should.be.true();
        locales.exists("invalid").should.be.false();
        locales.exists("en", "test").should.be.true();
        locales.exists("en_US").should.be.true();
        locales.exists("en_US", "test").should.be.true();
        locales.exists("en", "invalid").should.be.false();
        locales.exists("invalid", "invalid").should.be.false();


        locales.getLocales().should.deepEqual(["de", "en", "en_US"]);
    });
});

