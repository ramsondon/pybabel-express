const { Config } = require("./../../src/config/config");
const path = require("path");

describe("Config Test Suite", function () {

    it("test reading the config from a file", async function () {
        const config = Config.fromFile();
        config.sources.should.have.lengthOf(2);
        config.destination.should.equal("locales");
        config.babel.should.equal("babel.cfg");
        config.domain.should.equal("test");
        config.default_locale.should.equal("en");
    });


    it("test getSources", async function () {
        const config = new Config();

        config.sources = ["src/", "test/"];
        config.getSources().should.equal("src/ test/");

        config.sources = ["src", "test"];
        config.getSources().should.equal("src/ test/");

    });

    it ("test getPotFile", async function () {
        const config = new Config();
        config.domain = "test";
        config.destination = "locales";

        config.getPotFile().should.equal(path.resolve("./locales/test.pot"));
    });

    it ("should return the destination with / suffix", async function () {
        const config = new Config();

        config.destination = "locales";
        config.getDestination().should.equal("locales/");

        config.destination = "locales/";
        config.getDestination().should.equal("locales/");
    });

});

describe("test config validation test suite", function () {

    it ("validation from config file should work", async function () {
        const config = Config.fromFile();
        config.validate().should.be.true();
    });
});