# 2.3.0
    - Implemented getSupportedLocales() for the locale Middelware
    - added req.supported_locales = localeMiddleware.getSupportedLocales() when adding the locale middleware 
        to the request
    - updated dependencies:
        "eslint": "^6.4.0",
        "mocha": "^6.2.0",
        "node-mocks-http": "^1.8.0",
# 2.2.1
    - Moved Gettext into request context
    - implemented gMoFileCache
# 2.2.0
    - refactored Gettext to be initialized in application scope instead of request scope
    - translator middleware can now accept a factory instead of an object
# 2.1.0
    - state