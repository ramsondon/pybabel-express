const { TranslatorMiddleware, LocaleMiddleware } = require("./src/express/middlewares");
const { GettextTranslator } = require("./src/translator/translator");
const { Factory } = require("./src/factory");

module.exports = {
    LocaleMiddleware,
    TranslatorMiddleware,
    Translator: GettextTranslator,
    Factory,
};